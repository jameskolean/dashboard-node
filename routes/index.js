const combineRouters = require('koa-combine-routers')
const rootRouter = require('./root')
const metricsRouter = require('./metrics')
const { Counter, Gauge } = require('prom-client')
const { startTime } = require('pino-http')

const httpMetricsLabelNames = ['method', 'path']
const totalHttpRequestCount = new Counter({
  name: 'nodejs_http_total_count',
  help: 'total request number',
  labelNames: httpMetricsLabelNames,
})
const totalHttpRequestDuration = new Gauge({
  name: 'nodejs_http_total_duration',
  help: 'the last duration or response time of last request',
  labelNames: httpMetricsLabelNames,
})

function initMetrics4EachRoute(layer) {
  layer.stack.unshift(async (ctx, next) => {
    await next()
    totalHttpRequestCount.labels(ctx.method, layer.path).inc()
    // start time symbol defined in pino-http
    totalHttpRequestDuration
      .labels(ctx.method, layer.path)
      .inc(new Date().valueOf() - ctx.res[startTime])
  })
}

rootRouter.stack.forEach(initMetrics4EachRoute)

const router = combineRouters(rootRouter, metricsRouter)

module.exports = router
