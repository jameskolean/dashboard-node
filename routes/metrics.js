const Router = require('koa-router')
const { register } = require('prom-client')

const router = new Router()
router.get('/metrics', (ctx) => {
  ctx.headers['content-type'] = register.contentType
  ctx.body = register.metrics()
})

module.exports = router
