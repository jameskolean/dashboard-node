const { collectDefaultMetrics } = require('prom-client')
collectDefaultMetrics({ timeout: 5000 })
const Koa = require('koa')
const router = require('./routes')

const PORT = process.env.PORT || 3000

const app = new Koa()
app.use(router())

app.listen(PORT, () => {
  console.log('Server running on port ' + PORT)
})
